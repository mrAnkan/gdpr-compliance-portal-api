# Compliance Scanner
Use Compliance Scanner for finding personal-data in material from where it needs to be redacted. This tool uses NER to scan provided text for any personal-data. 

Use the provided companion add-on for Firefox to scan any of your web-based systems directly from your browser.

## Running app for the first time for development
Make sure to be in same dir as the `cli.py` file when running the commands below.

```
# python3 -m venv venv
# source venv/bin/activate
# pip install --upgrade pip
# pip install -r requirements.txt
# pip install pyinstaller
# python3 cli.py
```

Or just run the following commands using make:

```
# make initialize
# make install
# python3 cli.py
```

## Building app for release
Build for release relies on pyinstaller. The directory for `sacremoses` is for some reason ignored by pyinstaller, thus an additional manual step is required for copying that particular directory. 

Please make sure that python venv is activated before running any of the commands below (This can be done by running `make initialize`).

``` 
# pyinstaller --paths ./venv/lib/python3.8/site-packages cli.py
# cp -R ./venv/lib/python3.8/site-packages/sacremoses ./dist/cli/sacremoses
# chmod +x ./dist/cli/cli
``` 

All three of the commands above is boiled down to just the following using make:

``` 
# make build
``` 

For running the app just type `./dist/cli/cli`.

## Running Firefox add-on for development
Copy the manisfest file `./compliance_scanner/compliance_scanner.json` to the specific Firefox-location on your machine. You also need to edit the `path` attribute in the manifest to point to the precise location of the `cli.py` or the `dist/cli` file on your machine.

The required location for the manifest can be found at Mozilla documentation, https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_manifests#Manifest_location

For finding the settings-page for installing and debugging a temporary add-on, see a video in the following article over at Mozilla docs https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Modify_a_web_page  
Or just visit `about:debugging#/runtime/this-firefox` in  Firefox. 

## Acknowledgements
- Huge thanks to staff at Kungbib for creating and publishing Swedish BERT models used for Swedish NER, see https://github.com/Kungbib/swedish-bert-models
- The icon used is taken from the "GDPR Family" set by Lukasz Adam (https://www.iconfinder.com/lukaszadam), and is used on the basis that it is  free for commercial use, as long as a link to authors website is included.


