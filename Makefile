 # Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 # Copyright (C) 2020 Daniel Holm
 # 
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <https://www.gnu.org/licenses/>.

initialize:
	@echo "Initiating python virtual environment."
	python3 -m venv venv
	source venv/bin/activate

install:
	@echo "Installing dependencies."
	pip install --upgrade pip
	pip install -r requirements.txt
	pip install pyinstaller

build:
	@echo "Starting build of Compliance Scanner."
	pyinstaller --paths ./venv/lib/python3.8/site-packages cli.py
	cp -R ./venv/lib/python3.8/site-packages/sacremoses ./dist/cli/sacremoses
	chmod +x ./dist/cli/cli

clean:
	@echo "Removing ./build and ./dist directories."
	rm -r ./build
	rm -r ./dist
