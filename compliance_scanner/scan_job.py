 # Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 # Copyright (C) 2020 Daniel Holm
 # 
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
from dataclasses import dataclass, field
from datetime import datetime
import uuid

# Data class for holding information about a scan job.
@dataclass
class ScanJob():
  document: str
  reply_uri: str = ""
  external_id: str = ""
  job_id: uuid.UUID = field(default_factory=uuid.uuid4)
  created_at: datetime = field(default_factory=datetime.now)