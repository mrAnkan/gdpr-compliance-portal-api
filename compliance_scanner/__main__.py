 # Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 # Copyright (C) 2020 Daniel Holm
 # 
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
import sys
import json
import struct
import logging
import threading
import compliance_scanner
from compliance_scanner.scanner import Scanner
from transformers import pipeline

logging.basicConfig(filename='scanner.log',level=logging.DEBUG)

nlp = pipeline('ner', model='KB/bert-base-swedish-cased-ner', tokenizer='KB/bert-base-swedish-cased-ner')

# The current scanner instance.
current_scanner = Scanner(nlp)   

# Read a message from stdin and decode it.
def getMessage():
  rawLength = sys.stdin.buffer.read(4)

  if len(rawLength) == 0:
    sys.exit(0)

  messageLength = struct.unpack('@I', rawLength)[0]
  message = sys.stdin.buffer.read(messageLength).decode('utf-8')
  
  return json.loads(message)

# Encode a message for transmission,
# given its content.
def encodeMessage(messageContent):
  encodedContent = json.dumps(messageContent).encode('utf-8')
  encodedLength = struct.pack('@I', len(encodedContent))
    
  return { 'length': encodedLength, 'content': encodedContent }

# Send an encoded message to stdout.
def sendMessage(encodedMessage):
  sys.stdout.buffer.write(encodedMessage['length'])
  sys.stdout.buffer.write(encodedMessage['content'])
  sys.stdout.buffer.flush()

# Both encode and send message.
def performSendMessage(message):
  sendMessage(encodeMessage(message))

# The main loop for incoming messages.
def incomingLoop():
  logging.info("Started loop for receiving incomming messages.")

  while True:
    logging.info("The receiving-loop is waiting for incomming messages.")

    receivedMessage = getMessage()
    
    if receivedMessage["operation"] == "add":
      performSendMessage(current_scanner.add_scanning_job(receivedMessage["document"], receivedMessage["reply_uri"], receivedMessage["internal_id"]))
    
    if receivedMessage["operation"] == "remove":
      performSendMessage(current_scanner.delete_scanning_job(receivedMessage["job_id"]))
    
    if receivedMessage["operation"] == "get":
      performSendMessage(current_scanner.get_scanning_job(receivedMessage["job_id"]))

# The main loop for outgoing messages.
def outgoingLoop():
  logging.info("Started loop for sending messages.")

  while True:
    logging.info("The sending-loop is waiting for outgoing messages.")

    current = current_scanner.not_sent_job()

    if (current.reply_uri == "firefox-addon"):
      logging.info("The sending-loop is processing outgoing message for job_id: %s", str(current.job_id))
      performSendMessage(current_scanner.get_scanning_job(str(current.job_id)))

# Startup of compliance scanner.
def main():
  main = threading.Thread(name='main', target=incomingLoop)
  outgoing = threading.Thread(name='outgoing', target=outgoingLoop)
  scanner = threading.Thread(name='scanner', target=current_scanner.scanning_loop)
    
  scanner.start()
  main.start()
  outgoing.start()
    
  main.join()
  
# Start main loop and scanning loop.
if __name__ == "__main__":
  main()