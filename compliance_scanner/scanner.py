 # Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 # Copyright (C) 2020 Daniel Holm
 # 
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
import uuid
import queue
import logging
import threading
import compliance_scanner
from compliance_scanner.scan_job import ScanJob
from transformers import pipeline

print_lock = threading.Lock()

# Only allow one thread to print.
def sync_print(message):
    print_lock.acquire()
    print(message)
    print_lock.release()

proccess_queue = queue.Queue()
scanning_jobs = {}
scanning_results = {}
not_sent = queue.Queue()

# Class for execution of scan jobs.  
class Scanner():
  def __init__(self, nlp):
    self.nlp = nlp

  # perfom NER on provided document.
  def perform_ner(self, document):
    return self.nlp(document)

  # The loop that schedules scanning tasks to be performed.
  def scanning_loop(self):
    logging.info("Started a loop for scanning documents.")

    while True:
      logging.debug("Scanner waiting for document")
      
      scan_job = proccess_queue.get()

      logging.debug("Scanner got job: %s", scan_job.job_id)

      if scan_job == 'q':
          break
      else:
         logging.debug("Scanner is processing job with id: %s", scan_job.job_id)
         
         result = self.perform_ner(scan_job.document)
         
         sync_print(result)

         scanning_results[scan_job.job_id] = result
         
         if len(scan_job.reply_uri) > 0:
           not_sent.put(scan_job)
  
  # Automatic reply when reply uri is proivded.
  def not_sent_job(self): 
    return not_sent.get()

  # Queue a scanning job for processing.
  def add_scanning_job(self, document: str, reply_uri: str = "", external_id: str = ""):

    current_job = ScanJob(document, reply_uri, external_id)

    proccess_queue.put(current_job)
    scanning_jobs[current_job.job_id] = current_job

    logging.debug("Add scanning job resulted in id: %s", current_job.job_id)

    return { 'scanning_id:': str(current_job.job_id), 'internal_id': external_id }

  # Get processing details and result if any.
  def get_scanning_job(self, job_id: str):
    logging.debug("Request result for scanning job with id: %s", job_id)

    if uuid.UUID(job_id) in scanning_results:
      return { 
        'scanning_id': job_id, 
        'internal_id': scanning_jobs[uuid.UUID(job_id)].external_id, 
        'scanning_status': 'finished', 
        'result': scanning_results[uuid.UUID(job_id)] 
      }  
    
    return { 
      'scanning_id': job_id, 
      'scanning_status': 'waiting', 
      'result': None 
    }

  # Delete a pending scanning job, stop a job in progress and delete the result of a completed job.
  def delete_scanning_job(self, job_id: str):
    del scanning_jobs[job_id]  
  
