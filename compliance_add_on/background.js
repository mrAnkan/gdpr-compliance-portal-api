/**
 * Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// On startup, connect to the "compliance_scanner" companion app.
var port = browser.runtime.connectNative("compliance_scanner");

const waiting = [];
const scan = {};

/**
 * Send words to highlight to content script of specific tab.
 */
function sendHighlightCommand(tab, words) {
  browser.tabs.sendMessage(tab, {
      command: "highlight-text",
      words: words
  });
}

/**
 * Send statistics to popup.
 */
function sendStatisticsCommand() {
  browser.runtime.sendMessage({
    "destination": "compliance_popup",
    "operation": "received-scanning-result", 
    "statistics": {
      "waiting": waiting.length,
      "scanned": Object.keys(scan).length
    }
  });
}

/**
 * Listen for messages from the companion app.
 */
port.onMessage.addListener(async (applicationMessage) => {
  console.log("Background received an application message: ", applicationMessage);

  if(applicationMessage) {
    const id = applicationMessage.internal_id;

    // Ticket from companion app.
    if(!applicationMessage.result) {
      scan[id]["scanning_id"] = applicationMessage.scanning_id; 

      waiting.push(applicationMessage.scanning_id);
    }

    // Result of scan from companion app.
    if(applicationMessage.scanning_status == "finished") {
      scan[id]["result"] = applicationMessage.result;
      waiting.splice(applicationMessage.scanning_id, 1);

      sendHighlightCommand(scan[id].tab, applicationMessage.result);
    }
  }
});

/**
 * Listen for messages from popup and content script.
 */
browser.runtime.onMessage.addListener((message, sender) => { 
  if(message && message.destination === "background") {
    console.log('Background received a web message: ', message);
    
    switch (message.operation) {
      case "add":
        const id = 'scan_' + new Date().getUTCMilliseconds();
        
        scan[id] = { "tab": sender.tab.id };

        port.postMessage({ 
          "operation": "add", 
          "document": message.data, 
          "reply_uri": "firefox-addon", 
          "internal_id": id 
        });
        break;
      case "remove":
        port.postMessage({ "operation": "remove", "job_id": message.data });
        break;
      case "get":
        waiting.forEach(scanningId => {
          port.postMessage({ "operation": "get", "job_id": scanningId });
        });

        sendStatisticsCommand();
      default:
        break;
    }
  }
});
