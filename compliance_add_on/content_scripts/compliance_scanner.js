/**
 * Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

(function() {
  // Global guard variable.
  if (window.hasRun) {
    return;
  }
  window.hasRun = true;

  const markInstance = new Mark(document.querySelector("body"));
  const elementHighlighter = ElementHighlighter();

  /**
   * Send body text to background.
   */
  function sendBodyTextToBackground() {
    console.log(document.body.innerText);
    console.log(document.body.textContent);

    sendTextToBackground(document.body.textContent)   
  }

  /**
   * Get text of selected element.
   */
  function getSelectedText(element) {
    return element.textContent;
  }

  /**
   * Send text of selected element to background.
   */
  function sendSelectedTextToBackground(event) {
    
    const element = document.elementsFromPoint(event.clientX, event.clientY)[1];

    sendTextToBackground(getSelectedText(element));

    document.removeEventListener("click", sendSelectedTextToBackground);
    elementHighlighter.removeEventListenerElementHighlighter();
  }

  /**
   * Send text to background.
   */
  function sendTextToBackground(text) {
    browser.runtime.sendMessage({
      "destination": "background", 
      "operation": "add", 
      "data": text
    });
  }

  /**
   * Set listeners for element selection.
   */
  function setElementSelectionListener() {
    elementHighlighter.addEventListenerElementHighlighter();
    
    document.addEventListener('click', sendSelectedTextToBackground);

    document.addEventListener('keydown', function(event) {
      const key = event.key;
      console.log(key);
      
      if (key === "Escape") {
        document.removeEventListener("click", sendSelectedTextToBackground);
        elementHighlighter.removeEventListenerElementHighlighter();
      }
    });
  }

  /**
   * Highlights the words that were found.
   */
  function highlightText(words) {
    markInstance.unmark({
      done: function(){
        for(let i = 0; i < words.length; i++) {
          markInstance.mark(words[i].word);
        }
      }
    });
  }

  /**
   * Listen for messages from the compliance popup script.
   */
  browser.runtime.onMessage.addListener((message) => {
    console.log("Compliance Scanner received a tab message: ", message);

    if (message.command === "body-text") {
      sendBodyTextToBackground();
    }
    if (message.command === "select-text") {
      setElementSelectionListener();
    }
    if (message.command === "highlight-text") {
      highlightText(message.words)
    }
  });
})();
