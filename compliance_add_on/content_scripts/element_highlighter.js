/**
 * Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Self-managing element highlighter.
 * Most of the code below for highlighting the correct div is by AlienKevin 
 * from https://stackoverflow.com/a/55548782/3946538
 */
function ElementHighlighter() {
  let previousTarget;

  const hoverBox = document.createElement("div");
  hoverBox.style.position = "absolute";
  hoverBox.style.background = "rgba(153, 235, 255, 0.5)";
  hoverBox.style.zIndex = "0";

  document.body.appendChild(hoverBox);

  /**
   * Highlight element on hover.
   */
   function highlightElementOnHover(event) {
    
    let target = event.target;
  
    if(target === hoverBox) {
      
      const hoveredElement = document.elementsFromPoint(event.clientX, event.clientY)[1];
      
      if(previousTarget === hoveredElement) {
        return;
      } else {
        target = hoveredElement;
      }
    } else {    
      previousTarget = target;
    }
  
     // Add a border to hover box, using dimensions of the hover-target.  
    const targetOffset = target.getBoundingClientRect();
    const targetHeight = targetOffset.height;
    const targetWidth = targetOffset.width;
   
    const boxBorder = 5;
    hoverBox.style.width = targetWidth + boxBorder * 2 + "px";
    hoverBox.style.height = targetHeight + boxBorder * 2 + "px";
  
    // Position highlight, using scrollX and scrollY to account for scrolling.
    hoverBox.style.top = targetOffset.top + window.scrollY - boxBorder + "px";
    hoverBox.style.left = targetOffset.left + window.scrollX - boxBorder + "px";
  }

  return {
     addEventListenerElementHighlighter: function() {
       document.body.style.cursor="crosshair";
       document.addEventListener("mousemove", highlightElementOnHover );
     },
     removeEventListenerElementHighlighter: function() {
      hoverBox.style.width = "0px";
      hoverBox.style.height = "0px";
      hoverBox.style.left = "0";
      document.body.style.cursor= "auto";

      document.removeEventListener("mousemove", highlightElementOnHover );
    }
  }
}