/**
 * Compliance Scanner - Find personal-data in material from where it needs to be redacted.
 * Copyright (C) 2020 Daniel Holm
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

function formatBodyCommand(tab, words) {
  browser.tabs.sendMessage(tab, {
    command: "highlight-text",
    words: words
  });
}

function getBodyCommand(tabs) {
  browser.tabs.sendMessage(tabs[0].id, {
    command: "body-text"
  });
}

function selectTextCommand(tabs) {
  browser.tabs.sendMessage(tabs[0].id, {
    command: "select-text"
  });
}

function getOperation() {
  browser.runtime.sendMessage({ 
    "destination": "background",
    "operation": "get"
  });
}

function reportError(error) {
  console.error(`An error has occured: ${error}`);
}

function listenForClicks() {
  document.addEventListener("click", (e) => {
    if (e.target.classList.contains("add-compliance")) {
      browser.tabs.query({ active: true, currentWindow: true })
        .then(getBodyCommand)
        .catch(reportError);
    }
    if (e.target.classList.contains("get-compliance")) {
      getOperation();
    }
    if(e.target.classList.contains("select-element-scan")) {
      browser.tabs.query({ active: true, currentWindow: true })
        .then(selectTextCommand)
        .catch(reportError);
    }
  });
}

/**
 * Listen for messages from background.
 */
browser.runtime.onMessage.addListener((message) => { 
  if(message && message.destination === "compliance_popup") {
    console.log('Compliance Popup received a web message: ', message);
  
    if(message.operation === "received-scanning-result") {
      document.getElementById("currently-processing").innerText = message.statistics.waiting;  
      document.getElementById("already-processed").innerText = message.statistics.scanned; 
    }
  }
});

/**
 * Handle errors.
 */
function reportExecuteScriptError(error) {
  document.querySelector("#popup-content").classList.add("hidden");
  document.querySelector("#error-content").classList.remove("hidden");
  console.error(`Failed to execute content script: ${error.message}`);
}

/**
 * Inject a content script into the active tab. 
 */
 browser.tabs
.executeScript({file: "/content_scripts/compliance_scanner.js"})
.then(listenForClicks)
.catch(reportExecuteScriptError);

getOperation();